package com.sebastian.mpr;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class DatabaseInstanceTest {

    @Rule
    public TestRule watcher = new TestWatcher() {
        protected void starting(Description description) {
            System.out.println("Starting test: " + description.getMethodName());
        }
    };

    @Test
    public void itCorrectlyReturnsConnectionInstance() throws SQLException, ClassNotFoundException {
        assertNotNull(DatabaseInstance.getConnectionInstance());
    }

    @Test
    public void itCorrectlyReturnsStatementInstance() throws SQLException, ClassNotFoundException {
        assertNotNull(DatabaseInstance.getStatementInstance());
    }
}
