package com.sebastian.mpr.manager;

import com.sebastian.mpr.model.Episode;
import com.sebastian.mpr.model.Season;

import java.sql.SQLException;
import java.util.List;

public interface IEpisodeManager {
    void create(Episode episode) throws SQLException, ClassNotFoundException;
    void create(Season season, Episode episode) throws SQLException, ClassNotFoundException;
    List<Episode> getEpisodes() throws SQLException, ClassNotFoundException;
    Episode getLast() throws SQLException, ClassNotFoundException;
    List<Episode> getEpisodesForSeason(Season season) throws SQLException, ClassNotFoundException;
}
