package com.sebastian.mpr.manager;

import com.sebastian.mpr.model.Episode;
import com.sebastian.mpr.model.Season;

import java.sql.SQLException;
import java.util.List;

public interface ISeasonManager {
    void create(Season season) throws SQLException, ClassNotFoundException;
    List<Season> getSeasons() throws SQLException, ClassNotFoundException;
    Season getLast() throws SQLException, ClassNotFoundException;
}
