package com.sebastian.mpr.manager;

import com.sebastian.mpr.model.Director;

import java.sql.SQLException;
import java.util.List;

public interface IDirectorManager {
    void create(Director director) throws SQLException, ClassNotFoundException;
    List<Director> getDirectors() throws SQLException, ClassNotFoundException;
    Director getLast() throws SQLException, ClassNotFoundException;
}
