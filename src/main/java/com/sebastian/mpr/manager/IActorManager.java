package com.sebastian.mpr.manager;

import com.sebastian.mpr.model.Actor;

import java.sql.SQLException;
import java.util.List;

public interface IActorManager {
    void create(Actor actor) throws SQLException, ClassNotFoundException;
    List<Actor> getActors() throws SQLException, ClassNotFoundException;
    Actor getLast() throws SQLException, ClassNotFoundException;
}
