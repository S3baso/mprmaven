package com.sebastian.mpr;

import com.sebastian.mpr.manager.SeasonManager;

public class Main {
    public static void main(String[] args) {
        try {
            SeasonManager seasonManager = new SeasonManager();
            System.out.print(seasonManager.getSeasons().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
